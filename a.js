const crypto = require('crypto');

function setAlgorithm(){
}

 setAlgorithm.prototype.initial= function(algoName,password) {
  this.algoName = algoName;
  this.password =  password;
//console.log('Uma');
};

setAlgorithm.prototype.encrypt=function(text){

let cipher = crypto.createCipher(this.algoName, this.password);
let crypted = cipher.update(text, 'utf8', 'hex');
crypted += cipher.final('hex');
return crypted;

};

setAlgorithm.prototype.decrypt=function(text){

let decipher = crypto.createDecipher(this.algoName, this.password);
let dec = decipher.update(text, 'hex', 'utf8');
dec += decipher.final('utf8');
return dec;

};



module.exports=setAlgorithm;
