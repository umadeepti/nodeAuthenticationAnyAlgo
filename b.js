var enDe = require ('./a.js');
const xyz = require('./c');

var algo= new enDe();
console.log(" keys and algorithms");
console.log(xyz.cryptoObj);

var i=0, j=1, k=2 ;
//console.log("i="+i+"j="+j+"k="+k);

console.log("-------------------------------------------------------------");

//First Phase simple encryption-decryption
console.log("First Phase simple encryption-decryption: using first set key,algo encrypting-text=Iam testing ");
algo.initial(xyz.cryptoObj[i].algo,xyz.cryptoObj[i].key);
const txt1 =algo.encrypt('Iam testing');
console.log(txt1+'       [!!Encrypted-simple]');
const dtxt1= algo.decrypt(txt1);
console.log(dtxt1+'       [**Decrypted-simple]');

console.log("-------------------------------------------------------------");
//Second Phase double  encryption-decryption
console.log("Second  Phase double encryption-decryption: using second&third sets key,algo and encrypting-text=Iam multiple ");
console.log("-------------------------------------------------------------");
algo.initial('aes-128-cbc',xyz.cryptoObj[j].cryptoName);
const txt2=algo.encrypt('Iam multiple');
console.log(txt2+'!!Encrypted-once');
console.log("-------------------------------------------------------------");
algo.initial('aes-128-cbc',xyz.cryptoObj[k].cryptoName);
const txt21=algo.encrypt(txt2);
console.log(txt21+'!!Encrypted-twice');
console.log("-------------------------------------------------------------");
const dtxt21=algo.decrypt(txt21);
console.log(dtxt21+'**Decrypted-once');
console.log("-------------------------------------------------------------");
algo.initial('aes-128-cbc',xyz.cryptoObj[j].cryptoName);
const dtxt2=algo.decrypt(dtxt21);
console.log(dtxt2+'**Decrypted-twice');

//Done
console.log("DONE-SUCCESS");
